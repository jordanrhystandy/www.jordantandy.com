

module.exports.ASSETS_DIR = `${process.cwd()}/assets`;
module.exports.SASS_FILES = `${process.cwd()}/src/sass/**/*.scss`;
module.exports.DOC_FILES = `${process.cwd()}/src/docs/**/*.pdf`;
module.exports.FONT_FILES = `${process.cwd()}/src/fonts/*.{ttf,otf,woff,woff2,eot}`;
module.exports.JS_DIR = `${process.cwd()}/src/js`;
module.exports.JS_FILES = `${process.cwd()}/src/js/**/*.js`;
module.exports.IMG_FILES = `${process.cwd()}/src/img/**/*.{svg,jpg,png,gif,ico}`;
module.exports.IMG_ROOT = `${process.cwd()}/src/img`;
module.exports.SASS_ROOT = `${process.cwd()}/src/sass`;
module.exports.THEME_ROOT =  `${process.cwd()}/src/sass/themes`;