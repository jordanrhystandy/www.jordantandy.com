// TODO: SVG use and sprite
const gulp = require('gulp');
const {SASS_FILES, JS_FILES, IMG_FILES } = require('./config');


require('./tasks/css');
require('./tasks/webpack');
require('./tasks/images');
require('./tasks/fonts');
require('./tasks/clean');
require('./tasks/docs');

var devTasks = [];
var buildTasks = [];


gulp.task('watch', () => {
		gulp.watch(SASS_FILES, ['devCSS']);
		gulp.watch(IMG_FILES, ['devIMG']);
		gulp.watch(JS_FILES, ['webpack']);
	});


devTasks.push('webpack', 'watch', 'fonts', 'docs', 'devCSS','images');
// Make sure clean is fully executed before we do anything else

buildTasks.push('webpack:build', 'fonts','docs', 'buildCSS','images');

gulp.task('dev', ['clean'], function () {
	gulp.start.apply(this, devTasks);
});

gulp.task('build', ['clean'], function () { // Don't use an arrow function so the correct scope is retained for .apply()
	gulp.start.apply(this, buildTasks);
});
