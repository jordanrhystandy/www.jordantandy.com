const { ASSETS_DIR, JS_FILES } = require('../config');
const gulp = require('gulp');
const webpack = require('gulp-webpack');
const uglify = require('gulp-uglify-es').default;

gulp.task('webpack', () => {
	return gulp.src(JS_FILES)
		.pipe(webpack(require('../webpack.config.js')(false)))
		.pipe(gulp.dest(`${ASSETS_DIR}/script`));
});

gulp.task('webpack:build', () => {
	return gulp.src(JS_FILES)
		.pipe(webpack(require('../webpack.config.js')(true)))
		.pipe(uglify())
		.pipe(gulp.dest(`${ASSETS_DIR}/script`));
});
