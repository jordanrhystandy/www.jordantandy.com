const { ASSETS_DIR, SASS_FILES } = require('../config');
const gulp = require('gulp');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const cssOutputDir = `${ASSETS_DIR}/`;
const opDir = cssOutputDir + '/css'


gulp.task('devCSS', () => {

		return gulp.src(SASS_FILES)
			.pipe(sourcemaps.init())
			.pipe(sassGlob())
			.pipe(sass().on('error', sass.logError))
			.pipe(postcss([require('postcss-svg-fragments')]))
			.pipe(sourcemaps.write()).pipe(autoprefixer({
				browsers: ['> 5%', 'last 2 versions', 'not IE <= 10'] // http://browserl.ist/?q=%3E+5%25%2C+last+2+versions%2C+not+IE+%3C%3D+10
			}))
			.pipe(gulp.dest(opDir))
			
});


gulp.task('buildCSS', () => {

		return gulp.src(SASS_FILES)
			.pipe(sassGlob())
			.pipe(sass().on('error', sass.logError))
			.pipe(postcss([require('postcss-svg-fragments')]))
			.pipe(autoprefixer({
				browsers: ['> 5%', 'last 2 versions', 'not IE <= 10'] // http://browserl.ist/?q=%3E+5%25%2C+last+2+versions%2C+not+IE+%3C%3D+10
			}))
			.pipe(csso())
			.pipe(gulp.dest(opDir))
});
