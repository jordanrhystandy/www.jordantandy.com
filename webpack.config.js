const _ = require('lodash');
const webpack = require('webpack');
const { ASSETS_DIR, JS_DIR } = require('./config');

module.exports = (build) => {
	let extraConfig = {};

	// Optimised build
	if(build) {
		extraConfig = {
			plugins: [
				new webpack.DefinePlugin({
					'process.env.NODE_ENV': JSON.stringify('production')
				})
			]
		};
	// Dev build
	} else {
		extraConfig = {
			devtool: 'eval' // Include sourcemaps
		};
	}

	return _.extend({}, {
		entry: `${JS_DIR}/index.js`,
		output: {
			publicPath: '/assets/script/',
			filename: `[name].js`
		},
		plugins: [],
		module: {
			loaders: [
				{
					test: /\.js$/,
					loader: 'babel-loader',
					exclude: /node_modules/,
					query: {
						presets: [['es2015', { modules: false }]]
					}
				},
				{
					test: /\.hbs$/,
					loader: 'handlebars-loader'
				}
			]
		},
		resolve:
  {
    alias: {
      'handlebars' : 'handlebars/dist/handlebars.js'
    }
  },
	}, extraConfig);
};