import { confetti } from 'dom-confetti';
import stickybits from 'stickybits'

require('handlebars');
require('barba.js');

const scrollMonitor = require('scrollmonitor');
const Color = require('color');
const random = require('random');
const scrollToElement = require('scroll-to-element');
const Konami = require('konami');

const currentTime = new Date()
const yearEl = document.querySelector('.js-year');
const profileEl = document.querySelector('.sidebar__profile');
const scrollBtn = document.querySelector('.js-contact-scroll');
const toTopBtn = document.querySelector('.js-to-top');
const contactForm = document.querySelector('.info-section--contact');
const skillPills = [...document.querySelectorAll('.js-colors li')];

let brandPurple = Color('rgb(255, 0, 255)');

yearEl.innerHTML = currentTime.getFullYear();

scrollBtn.addEventListener("click", contactClick);
toTopBtn.addEventListener("click", toTopClick);

skillPills.forEach(pill => {
   let lightOrDark = random.integer(0, 1);

   if (lightOrDark == 1) {
      let newColor = brandPurple.lighten(parseFloat(random.float(0, 0.3).toFixed(1)));
      pill.style.backgroundColor = newColor;
   } else {
      let newColor = brandPurple.darken(parseFloat(random.float(0, 0.3).toFixed(1)));
      pill.style.backgroundColor = newColor;
   }
});



var toTopChecker = debounce(() => {
   if (window.scrollY > 100) {
      toTopBtn.parentNode.classList.add('is-visible');
   } else {
      toTopBtn.parentNode.classList.remove('is-visible');
   }
}, 50);

window.addEventListener('scroll', toTopChecker);

// -------------------- SCROLL MONITOR START -------------------- //

// controls the visibility of the sections depending on scroll pos.
const sectionElements = [...document.querySelectorAll('.info-section')];

sectionElements.forEach(element => {
   let watcher = scrollMonitor.create(element, {
      top: -100
   });
   watcher.enterViewport(function () {
      const skills = element.querySelectorAll('.skills-list li')
      element.classList.add('is-in-view')
      if(skills != null && skills.length > 0){
         animateNextPill();
      }
   });
});

// -------------------- SCROLL MONITOR END -------------------- //

stickybits(profileEl, {
   stickyBitStickyOffset: 25
});


// -------------------- SCROLLER START -------------------- //

function contactClick(e) {
   e.preventDefault();
   scrollToElement(contactForm, {
      offset: 0,
      ease: 'out-quart',
      duration: 800
   });
}

function toTopClick(e) {
   e.preventDefault();
   scrollToElement(document.documentElement, {
      offset: 0,
      ease: 'out-quart',
      duration: 800,
      align:'top'
   });

}
// -------------------- SCROLLER END -------------------- //

// -------------------- DEBOUNCER START -------------------- //

function debounce(func, wait, immediate) {
   var timeout;
   return function () {
      var context = this,
         args = arguments;
      var later = function () {
         timeout = null;
         if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
   };
};

// -------------------- DEBOUNCER END -------------------- //

toTopChecker();



function animateNextPill() {
   var elements = document.querySelectorAll('.skills-list li:not(.show)');
   if (elements.length) {
      elements[0].classList.add("show");
      delay(20).then(animateNextPill);
   }
}


function delay(ms) {
   var ctr,
      rej,
      p = new Promise(function(resolve, reject) {
         ctr = setTimeout(resolve, ms);
         rej = reject;
      });
   p.cancel = function() {
      clearTimeout(ctr);
      rej(Error("Cancelled"));
   };
   return p;
}

// -------------------- Easter Egg -------------------- //
// -------------------- Hint: Up, Up, Down, Down, Left, Right, Left, Right, B, A -------------------- //
console.info('Hey! I see you there, poking at my code! Want the full source code? Check it out at: https://bitbucket.org/jordanrhystandy/www.jordantandy.com/src/master/ 💖')
var easter_egg = new Konami(function(){
   const config = {
      angle: 90,
      spread: 45,
      startVelocity: 45,
      elementCount: 50,
      decay: 0.9,
      width: "10px",
      height: "10px"
    };
 const sections =  [...document.querySelectorAll('section')];
 sections.forEach(element => {
  delay(100).then(confetti(element, config));
});
});
