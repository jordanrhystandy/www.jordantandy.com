const { ASSETS_DIR, IMG_FILES } = require('../config');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const imgOutputDir = `${ASSETS_DIR}/`;
const opDir = imgOutputDir + '/img'

gulp.task('images', () => {
	return gulp.src(IMG_FILES)
		.pipe(imagemin([
			imagemin.jpegtran(),
      imagemin.gifsicle(),
			imagemin.optipng(),
			imagemin.svgo({plugins: [{cleanupIDs: false}]})
		]))
		.pipe(gulp.dest(opDir));
});

