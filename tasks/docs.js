const { ASSETS_DIR, DOC_FILES } = require('../config');
const gulp = require('gulp');


gulp.task('docs', () => {
	return gulp.src(DOC_FILES)
		.pipe(gulp.dest(`${ASSETS_DIR}/docs`));
});
