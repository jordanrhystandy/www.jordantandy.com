const { ASSETS_DIR, FONT_FILES } = require('../config');
const gulp = require('gulp');


gulp.task('fonts', () => {
	return gulp.src(FONT_FILES)
		.pipe(gulp.dest(`${ASSETS_DIR}/fonts`));
});
